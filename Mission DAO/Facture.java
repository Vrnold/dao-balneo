package iti.java3d.mission2.modele;

import java.sql.Date;

public class Facture {
	private int m_fact_no;
	private Date m_fact_date;
	private int m_fact_montant;
	private int m_solde;
	
	public Facture(int fact_no, Date fact_date, int fact_montant ) {
		m_fact_no = fact_no;
		m_fact_date= fact_date;
		m_fact_montant = fact_montant;
		
	}
	public int getM_fact_no() {
		return m_fact_no;
	}
	public void setM_fact_no(int m_fact_no) {
		this.m_fact_no = m_fact_no;
	}
	public Date getM_fact_date() {
		return m_fact_date;
	}
	public void setM_fact_date(Date m_fact_date) {
		this.m_fact_date = m_fact_date;
	}
	public int getM_fact_montant() {
		return m_fact_montant;
	}
	public void setM_fact_montant(int m_fact_montant) {
		this.m_fact_montant = m_fact_montant;
	}
	public int getM_solde() {
		return m_solde;
	}
	public void setM_solde(int m_solde) {
		this.m_solde = m_solde;
	}
	
	
}
