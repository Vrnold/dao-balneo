package iti.java3d.mission2.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import iti.java3d.mission2.modele.Facture;

public class FactureDAO extends DAO implements IFactureDAO {

	@Override
	public Facture getFactureByID(Facture  f) {

		Facture fresult=null;

		try {
			Connection conn=this.getConnection();
			int factID = f.getM_fact_no();
			String req2 = "SELECT fact_no, fact_date, fact_montant FROM Balneo_Facture";
			PreparedStatement pstmt2 = conn.prepareStatement(req2);
			pstmt2.setInt(1, factID);
			ResultSet result = pstmt2.executeQuery(req2);
			System.out.println(result);

			if(result.next())
				fresult= new Facture (result.getInt(1),result.getDate(2),result.getInt(3));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return fresult;

	}

	@Override
	public ArrayList<Facture> getAllFacture(Facture f) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteFacture(Facture f) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addFacture(Facture f) {
		// TODO Auto-generated method stub
		return false;
	}

}

