package iti.java3d.mission2.dao;

import java.util.ArrayList;

import iti.java3d.mission2.modele.Facture;

public abstract interface IFactureDAO {


		public Facture getFactureByID(Facture f);
		
		public ArrayList <Facture> getAllFacture(Facture f);

		public boolean deleteFacture (Facture f);

		public boolean addFacture(Facture f);
			
	}

