package iti.java3d.mission2.dao;

import java.util.ArrayList;

import iti.java3d.mission2.modele.Facture;
import iti.java3d.mission2.modele.Paiement;

public  interface IPaiementDAO {

	public Paiement getPaiementByID(Paiement p);
		
	public ArrayList <Paiement>getPaiementByFacture( Facture f);
	
	public boolean deletePaiementFacture(Paiement p, Facture f);
	
	public boolean addPaiementFacture(Paiement p, Facture f);
}
