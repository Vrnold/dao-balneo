package iti.java3d.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.Scanner;

public class JDBCPrepare {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		/******************Connexion a la base de donnees***********************/
		String driver="com.mysql.cj.jdbc.Driver";
		String urlBDD="jdbc:mysql://sl-eu-lon-2-portal.3.dblayer.com:21952 /TestSD"; 
		String user="admin"; //login de connexion au serveur MySQL
		String pwd="WPSPLUXUUJXZYRSX"; //mdp correspondant Class.forName(driver); 
		Class.forName(driver);
		System.out.println("driver ok"); 
		
		Connection conn = DriverManager.getConnection(urlBDD,user,pwd); 
		System.out.println("connection ok"); 
		/***********************************************************************/
		
		/*******************Création de la requête préparée**************************/
		String reqP = "INSERT INTO Balneo_Facture VALUES (?,?,?) ";
		PreparedStatement psmt = conn.prepareStatement(reqP);
		/****************************************************************************/
		
		// Preparation de la requête
		Scanner sc = new Scanner(System.in);
		System.out.println("-----------------Insertion facture--------------------");
		System.out.print("fact_no : ");
		int fact_no = sc.nextInt();
		psmt.setInt(1, fact_no);
		System.out.print("facture date yyyy-mm-dd: ");
		String date = sc.next();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
		//String date1 = dateFormat.format(date);
		
		Date fact_date=null;
		try {
		    //Parsing the String
		    fact_date = (Date) dateFormat.parse(date);
		} catch (ParseException e) {
		    
			System.out.println("format date mal saisie : yyyy-mm-dd");
		}
		psmt.setDate(2,fact_date);
		
		System.out.print("fact_montant : ");
		int fact_montant = sc.nextInt();
		psmt.setInt(3, fact_montant);
		
		int res2 = psmt.executeUpdate();
		System.out.println("nb de modifications realisées : "+res2);
		
		sc.close();
		}catch(SQLException | ClassNotFoundException e){ 
			e.printStackTrace(); 
			
		}

		
	}

}

/*

public class iti_java3d_dao {
    public void getConnexion() {
        try{
        String driver="com.mysql.cj.jdbc.Driver"; //disponible sur le site MySQL
        String urlBDD="jdbc:mysql://sl-eu-lon-2-portal.3.dblayer.com:21952/BalneoDiouf_Sam ";
        String user="admin"; //login de connexion au serveur MySQL
        String pwd="WPSPLUXUUJXZYRSX"; //mdp correspondant
        Class.forName(driver);
        System.out.println("driver ok");
        Connection conn= DriverManager.getConnection(urlBDD,user,pwd);
        System.out.println("connection ok");
        //suite du code
        /*
        //Insertion d'une ligne
        Statement stmt = conn.createStatement();
        String req1 = "INSERT INTO Balneo_Facture values (1205, '2012-07-12', 5000)";
        int res = stStatement stmt = conn.createStatement();mt.executeUpdate(req1);
        System.out.println("nb de modifications réalisées : " + res);
        
        
        //Affichage des factures 
        Statement stmt = conn.createStatement();
        String req2 = "SELECT fact_no, fact_date,fact_montant FROM Balneo_Facture";
        ResultSet result = stmt.executeQuery(req2);
        while(result.next()){(iti_java3d_modele) 
            System.out.print("fact_no : "+ result.getInt(1));
            System.out.print("  fact_date : "+ result.getDate(2));
            System.out.println("  fact_montant : "+ result.getInt(3));
        
        
        
        //Execution requête SQL
           String req1 = "INSERT INTO Balneo_Facture values (1895, '2015-10-18', 5052)"; // création de l'objet qui contient la reqête SQL
           Statement stmt = conn.createStatement();
           int res = stmt.executeUpdate(req1); //Mise à jour de notre table
           
           System.out.println("nb de modifications réalisées : " + res);
           
           //SelectionFacture
           //Exécution d’une requête de sélection (SELECT)
           String req2 = "SELECT fact_no, fact_date, fact_montant FROM Balneo_Facture";
           ResultSet result = stmt.executeQuery(req2);
           ArrayList<iti_java3d_modele> al = new ArrayList<iti_java3d_modele>();
           
           while(result.next()) {
            iti_java3d_modele x = new iti_java3d_modele (result.getInt(1),result.getDate(2),result.getInt(3));
            al.add(x);
            System.out.println(x.toString());
            
        }
        
        result.close();
        stmt.close();
        
}
catch(SQLException | ClassNotFoundException e){
        e.printStackTrace();
}
}
}
*/
